package org.xelevra.dblog;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;


public final class DatabaseLog {
    final DBOpenHelper dbOpenHelper;
    private final Scheduler scheduler;
    private volatile boolean disabled;


    private final Observable<List<String>> coldTagsGetter;
    private final Observable<List<LogMessage>> coldAllGetter;
    private final Observable<Boolean> coldClearer;

    private final Subject<LogMessage> newMessages, newMessagePutter;

    public DatabaseLog(Context context, int maxRows) {
        this(context, maxRows, Schedulers.io());
    }

    DatabaseLog(Context context, int maxRows, Scheduler scheduler){
        dbOpenHelper = new DBOpenHelper(context, maxRows);
        this.scheduler = scheduler;

        newMessages = PublishSubject.<LogMessage>create().toSerialized();
        newMessagePutter = PublishSubject.<LogMessage>create().toSerialized();
        newMessagePutter.observeOn(scheduler).filter(new Predicate<LogMessage>() {
            @Override
            public boolean test(@NonNull LogMessage logMessage) throws Exception {
                return !disabled;
            }
        }).doOnNext(new Consumer<LogMessage>() {
            @Override
            public void accept(LogMessage logMessage) throws Exception {
                dbOpenHelper.putMessage(logMessage.tag, logMessage.message);
            }
        }).subscribe(newMessages);

        coldTagsGetter = Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<List<String>> emitter) throws Exception {
                Cursor cursor = dbOpenHelper.getTags();
                List<String> result = new ArrayList<String>(cursor.getCount());
                final int index = cursor.getColumnIndex(DBOpenHelper.C_TAG);
                while (cursor.moveToNext()) result.add(cursor.getString(index));
                cursor.close();
                emitter.onNext(result);
            }
        }).subscribeOn(scheduler);

        coldAllGetter = Observable.create(new ObservableOnSubscribe<Cursor>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Cursor> emitter) throws Exception {
                emitter.onNext(dbOpenHelper.getMessages(null));
            }
        }).map(mapFromCursor).subscribeOn(scheduler);

        coldClearer = Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Boolean> emitter) throws Exception {
                dbOpenHelper.clear(null);
                emitter.onNext(Boolean.TRUE);
            }
        }).subscribeOn(scheduler);
    }

    public Observable<List<String>> getTags() {
        return newMessages.flatMap(new Function<LogMessage, Observable<List<String>>>() {
            @Override
            public Observable<List<String>> apply(@NonNull LogMessage logMessage) throws Exception {
                return coldTagsGetter;
            }
        }).mergeWith(coldTagsGetter);
    }

    public Observable<List<LogMessage>> getLog(final String tag) {
        return Observable.create(new ObservableOnSubscribe<Cursor>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Cursor> emitter) throws Exception {
                emitter.onNext(dbOpenHelper.getMessages(tag));
            }
        }).map(mapFromCursor)
                .subscribeOn(scheduler)
                .mergeWith(
                        newMessages.filter(new Predicate<LogMessage>() {
                            @Override
                            public boolean test(@NonNull LogMessage logMessage) throws Exception {
                                return logMessage.tag == null || logMessage.tag.equals(tag);
                            }
                        }).map(new Function<LogMessage, List<LogMessage>>() {
                            @Override
                            public List<LogMessage> apply(@NonNull LogMessage logMessage) throws Exception {
                                if(logMessage.getMessage() == null) return Collections.emptyList();
                                return Collections.singletonList(logMessage);
                            }
                        })
                );

    }

    public Observable<List<LogMessage>> getAllLogs() {
        return coldAllGetter.mergeWith(
                newMessages.flatMap(new Function<LogMessage, ObservableSource<List<LogMessage>>>() {
                    @Override
                    public ObservableSource<List<LogMessage>> apply(@NonNull LogMessage logMessage) throws Exception {
                        if(logMessage.getMessage() == null) return Observable.empty();
                        else return Observable.just(Collections.singletonList(logMessage));
                    }
                })
        );
    }

    public void message(String tag, String message) {
        newMessagePutter.onNext(new LogMessage(tag, message));
    }

    public void clear(final String tag) {
        Observable.<Boolean>create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Boolean> emitter) throws Exception {
                dbOpenHelper.clear(tag);
                emitter.onNext(Boolean.TRUE);
            }
        }).subscribeOn(scheduler).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) {
                newMessages.onNext(new LogMessage(tag, null));
            }
        });
    }

    public void clear() {
        coldClearer.subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) {
                newMessages.onNext(new LogMessage());
            }
        });
    }

    public void setEnabled(boolean enabled){
        disabled = !enabled;
    }

    private static final Function<Cursor, List<LogMessage>> mapFromCursor = new Function<Cursor, List<LogMessage>>() {
        @Override
        public List<LogMessage> apply(Cursor cursor) {
            List<LogMessage> result = new ArrayList<LogMessage>(cursor.getCount());
            final int tagIndex = cursor.getColumnIndex(DBOpenHelper.C_TAG);
            final int messageIndex = cursor.getColumnIndex(DBOpenHelper.C_MESSAGE);
            final int timestampIndex = cursor.getColumnIndex(DBOpenHelper.C_TIMESTAMP);
            LogMessage message;
            while (cursor.moveToNext()) {
                message = new LogMessage();
                message.tag = cursor.getString(tagIndex);
                message.message = cursor.getString(messageIndex);
                message.timestamp = cursor.getLong(timestampIndex);
                result.add(message);
            }
            return result;
        }
    };
}
