package org.xelevra.dblog;


import java.util.Date;

public class LogMessage {
    String tag;
    long timestamp;
    String message;

    public LogMessage(){ }

    public LogMessage(String tag, String message){
        this.tag = tag;
        this.message = message;
        this.timestamp = System.currentTimeMillis();
    }

    public Date getDate(){
        return new Date(timestamp);
    }

    public String getTag(){
        return tag;
    }

    public String getMessage(){
        return message;
    }

    @Override
    public String toString() {
        return tag + ":" + message;
    }
}
