package org.xelevra.dblog;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class DBOpenHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 5;

    public static final String C_ID = "c_id";

    public static final String T_LOG = "log";

    public static final String C_TAG = "c_tag";
    public static final String C_TIMESTAMP = "C_timestamp";
    public static final String C_MESSAGE = "c_message";
    public static final String TR_DELETE_LOG = "t_delete_log";


    private final long MAX_LOG_ROWS;

    public DBOpenHelper(Context context, long maxRows) {
        super(context, "db_log", null, DB_VERSION);
        if(maxRows < 1) throw new IllegalArgumentException("Count of rows must be >= 1");
        SharedPreferences preferences = context.getSharedPreferences("dblog", Context.MODE_PRIVATE);
        MAX_LOG_ROWS = maxRows;

        if (preferences.getLong("max_rows", -1) != maxRows){
            rewriteTrigger();
            preferences.edit().putLong("max_rows", maxRows).apply();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + T_LOG + "("
                + C_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + C_TAG + " TEXT NOT NULL, "
                + C_TIMESTAMP + " INTEGER NOT NULL, "
                + C_MESSAGE + " TEXT NOT NULL"
                + ")"
        );

        createTrigger(db);
    }

    private void rewriteTrigger(){
        Log.i("DBLog", "Require to replace the trigger");
        if(getWritableDatabase() != null) {
            Log.i("DBLog", "Perform replacing the trigger");
            getWritableDatabase().execSQL("DROP TRIGGER IF EXISTS " + TR_DELETE_LOG);
            createTrigger(getWritableDatabase());
        }
    }

    private void createTrigger(SQLiteDatabase db){
        db.execSQL(
                "CREATE TRIGGER " + TR_DELETE_LOG + " AFTER INSERT ON " + T_LOG
                        + " BEGIN "
                        +" DELETE FROM " + T_LOG
                        + " WHERE "
                        + C_TAG + " = NEW." + C_TAG + " AND "
                        + C_ID + " NOT IN ("
                        + "SELECT " + C_ID + " FROM ("
                        + "SELECT " + C_ID + " FROM " + T_LOG
                        + " WHERE " + C_TAG + " = NEW." + C_TAG
                        + " ORDER BY " + C_ID + " DESC"
                        + " LIMIT " + MAX_LOG_ROWS
                        + ")"
                        + ");"
                        + "END"
        );
    }

    void putMessage(String tag, String message){
        ContentValues values = new ContentValues(3);
        values.put(C_TAG, tag);
        values.put(C_MESSAGE, message);
        values.put(C_TIMESTAMP, System.currentTimeMillis());
        getReadableDatabase().insert(T_LOG, "", values);
    }

    Cursor getMessages(String tag){
        String condition = tag == null ? null : C_TAG + " = \"" + tag + "\"";
        return getReadableDatabase().query(T_LOG, null, condition, null, null, null, C_ID);
    }

    Cursor getTags(){
        return getReadableDatabase().query(true, T_LOG, new String[]{C_TAG}, null, null, null, null, null, null);
    }

    void clear(String tag){
        String condition = tag == null ? null : C_TAG + " = \"" + tag + "\"";
        getReadableDatabase().delete(T_LOG, condition, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TRIGGER IF EXISTS " + TR_DELETE_LOG);
        db.execSQL("DROP TABLE IF EXISTS " + T_LOG + "; VACUUM");
        onCreate(db);
    }
}
