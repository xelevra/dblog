package org.xelevra.dblog;

import android.support.annotation.NonNull;
import com.pushtorefresh.storio.sqlite.operations.delete.DefaultDeleteResolver;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;

/**
 * Generated resolver for Delete Operation
 */
class LogMessageStorIOSQLiteDeleteResolver extends DefaultDeleteResolver<LogMessage> {
    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    protected DeleteQuery mapToDeleteQuery(@NonNull LogMessage object) {
        throw new UnsupportedOperationException();
    }
}
