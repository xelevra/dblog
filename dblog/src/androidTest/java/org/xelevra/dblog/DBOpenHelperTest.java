package org.xelevra.dblog;

import android.test.AndroidTestCase;

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.schedulers.ImmediateThinScheduler;


public class DBOpenHelperTest extends AndroidTestCase {

    private static final String TAG = "test";
    private static final int MAX_ROWS = 10;
    private DatabaseLog log;
    private StorIOSQLite db;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        log = new DatabaseLog(getContext(), MAX_ROWS, ImmediateThinScheduler.INSTANCE);
        db = DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(log.dbOpenHelper)
                .addTypeMapping(LogMessage.class, SQLiteTypeMapping.<LogMessage>builder()
                        .putResolver(new LogMessageStorIOSQLitePutResolver())
                        .getResolver(new LogMessageStorIOSQLiteGetResolver())
                        .deleteResolver(new LogMessageStorIOSQLiteDeleteResolver())
                        .build()
                ).build();
    }

    public void testLogTableCircleBuffer(){
        for (int i = 0; i < MAX_ROWS; i++){
            db.put().object(new LogMessage(TAG, String.valueOf(i))).prepare().executeAsBlocking();
        }

        List<LogMessage> messages = db.get().listOfObjects(LogMessage.class).withQuery(
                Query.builder().table(DBOpenHelper.T_LOG).build()
        ).prepare().executeAsBlocking();
        assertEquals(messages.size(), MAX_ROWS);
        db.put().object(new LogMessage(TAG, "final")).prepare().executeAsBlocking();
        db.put().object(new LogMessage("anotherTag", "another")).prepare().executeAsBlocking();
        messages = db.get().listOfObjects(LogMessage.class).withQuery(
                Query.builder().table(DBOpenHelper.T_LOG).where(DBOpenHelper.C_TAG + " = \"" + TAG + "\"").build()
        ).prepare().executeAsBlocking();
        assertEquals(MAX_ROWS, messages.size());
        assertEquals("1", messages.get(0).message);
        assertEquals("final", messages.get(MAX_ROWS - 1).message);
    }

    public void testGetNames() throws Exception {
        log.message("1", "11");
        log.message("1", "12");
        log.message("2", "21");

        final AtomicReference<List<String>> tags = new AtomicReference<>();
        log.getTags().subscribe(new Consumer<List<String>>() {
            @Override
            public void accept(List<String> strings) {
                tags.set(strings);
            }
        });

        assertNotNull(tags.get());
        assertEquals(2, tags.get().size());
        assertTrue(tags.get().contains("1"));
        assertTrue(tags.get().contains("2"));

        log.message("3", "31");

        assertNotNull(tags.get());
        assertEquals(3, tags.get().size());
        assertTrue(tags.get().contains("1"));
        assertTrue(tags.get().contains("2"));
        assertTrue(tags.get().contains("3"));
    }

    public void testGetAllMessages() throws Exception{
        log.message("1", "11");
        log.message("1", "12");
        log.message("2", "21");

        final AtomicReference<List<LogMessage>> messages = new AtomicReference<>();

        log.getAllLogs().subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                messages.set(logMessages);
            }
        });

        assertNotNull(messages.get());
        assertEquals(3, messages.get().size());

        messages.set(null);
        log.message("2", "22");
        assertNotNull(messages.get());
        assertEquals(1, messages.get().size());
        assertEquals("22", messages.get().get(0).message);

        messages.set(null);
        log.message("1", "13");
        assertNotNull(messages.get());
        assertEquals(1, messages.get().size());
        assertEquals("13", messages.get().get(0).message);
    }

    public void testGetMessagesByTag() throws Exception{
        db.put().object(new LogMessage("1", "11")).prepare().executeAsBlocking();
        db.put().object(new LogMessage("1", "12")).prepare().executeAsBlocking();
        db.put().object(new LogMessage("2", "21")).prepare().executeAsBlocking();

        final AtomicReference<List<LogMessage>> messages = new AtomicReference<>();

        log.getLog("1").subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                messages.set(logMessages);
            }
        });

        assertNotNull(messages.get());
        assertEquals(2, messages.get().size());

        messages.set(null);
        log.message("2", "22");
        assertNull(messages.get());
        log.message("1", "13");
        assertNotNull(messages.get());
        assertEquals(1, messages.get().size());
        assertEquals("13", messages.get().get(0).message);
    }

    public void testAsync() throws Exception{
        DatabaseLog log = new DatabaseLog(getContext(), MAX_ROWS);
        final Semaphore semaphore = new Semaphore(1);
        semaphore.acquire(1);
        final AtomicReference<String> threadName = new AtomicReference<>();

        Disposable s = log.getTags().observeOn(ImmediateThinScheduler.INSTANCE).subscribe(new Consumer<List<String>>() {
            @Override
            public void accept(List<String> logMessages) {
                threadName.set(Thread.currentThread().getName());
                semaphore.release();
            }
        });

        log.message("test", "test");
        assertTrue(semaphore.tryAcquire(1, TimeUnit.SECONDS));

        assertNotNull(threadName.get());
        assertFalse(Thread.currentThread().getName().equals(threadName.get()));

        s.dispose();

        log.getAllLogs().observeOn(ImmediateThinScheduler.INSTANCE).subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                threadName.set(Thread.currentThread().getName());
                semaphore.release();
            }
        });

        log.message("test", "test");
        assertTrue(semaphore.tryAcquire(1, TimeUnit.SECONDS));

        assertNotNull(threadName.get());
        assertFalse(Thread.currentThread().getName().equals(threadName.get()));
    }

    public void testClearAll() throws Exception{
        db.put().object(new LogMessage("1", "11")).prepare().executeAsBlocking();
        db.put().object(new LogMessage("2", "21")).prepare().executeAsBlocking();

        final AtomicReference<List<LogMessage>> messages = new AtomicReference<>();

        final AtomicInteger callsAll = new AtomicInteger(0), calls2 = new AtomicInteger(0), calls1 = new AtomicInteger(0);

        log.getAllLogs().subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                callsAll.incrementAndGet();
                messages.set(logMessages);
            }
        });

        log.getLog("1").subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                calls1.incrementAndGet();
            }
        });
        log.getLog("2").subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                calls2.incrementAndGet();
            }
        });


        assertEquals(1, callsAll.get());
        assertEquals(1, calls1.get());
        assertEquals(1, calls2.get());
        log.clear();

        assertEquals(1, callsAll.get());
        assertEquals(2, calls1.get());
        assertEquals(2, calls2.get());

        log.getAllLogs().subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                callsAll.incrementAndGet();
                messages.set(logMessages);
            }
        });
        assertNotNull(messages.get());
        assertTrue("Messages size is " + messages.get().size(), messages.get().isEmpty());
    }

    public void testClear() throws Exception{
        log.message("1", "11");
        log.message("2", "21");

        final AtomicReference<List<LogMessage>> messages = new AtomicReference<>();

        log.getAllLogs().subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                messages.set(logMessages);
            }
        });

        assertNotNull(messages.get());
        assertEquals(2, messages.get().size());
        messages.set(null);

        log.clear("1");
        assertNull(messages.get());
        log.getAllLogs().subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                messages.set(logMessages);
            }
        });
        assertNotNull(messages.get());
        assertEquals(1, messages.get().size());
        assertEquals("2", messages.get().get(0).tag);
    }

    public void testEmptyOnClear() throws Exception {
        log.message("1", "11");
        log.message("2", "21");

        final AtomicReference<List<LogMessage>> messages = new AtomicReference<>();

        log.getLog("1").subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                messages.set(logMessages);
            }
        });

        assertNotNull(messages.get());
        assertEquals(1, messages.get().size());
        messages.set(null);

        log.clear("1");
        assertNotNull(messages.get());
        assertTrue(messages.get().isEmpty());
    }

    public void testDisable(){
        final AtomicReference<List<LogMessage>> messages = new AtomicReference<>();

        log.getAllLogs().subscribe(new Consumer<List<LogMessage>>() {
            @Override
            public void accept(List<LogMessage> logMessages) {
                messages.set(logMessages);
            }
        });

        log.setEnabled(false);

        log.message("TEST", "TEST");
        assertNotNull(messages.get());
        assertTrue(messages.get().isEmpty());

        log.setEnabled(true);

        log.message("TEST", "TEST");
        assertNotNull(messages.get());
        assertEquals(1, messages.get().size());
    }

    @Override
    protected void tearDown() throws Exception {
        db.delete().byQuery(DeleteQuery.builder().table(DBOpenHelper.T_LOG).build()).prepare().executeAsBlocking();
        assertEquals(Integer.valueOf(0), db.get().numberOfResults().withQuery(Query.builder().table(DBOpenHelper.T_LOG).build()).prepare().executeAsBlocking());
        super.tearDown();
    }
}