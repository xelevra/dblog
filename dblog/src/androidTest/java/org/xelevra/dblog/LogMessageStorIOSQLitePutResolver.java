package org.xelevra.dblog;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio.sqlite.queries.UpdateQuery;

/**
 * Generated resolver for Put Operation
 */
class LogMessageStorIOSQLitePutResolver extends DefaultPutResolver<LogMessage> {
    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    protected InsertQuery mapToInsertQuery(@NonNull LogMessage object) {
        return InsertQuery.builder()
            .table("log")
            .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    protected UpdateQuery mapToUpdateQuery(@NonNull LogMessage object) {
        return UpdateQuery.builder()
            .table("log")
            .where(DBOpenHelper.C_TIMESTAMP + " = ?")
            .whereArgs(object.timestamp)
            .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public ContentValues mapToContentValues(@NonNull LogMessage object) {
        ContentValues contentValues = new ContentValues(4);

        contentValues.put("c_message", object.message);
        contentValues.put("c_tag", object.tag);
        contentValues.put("C_timestamp", object.timestamp);

        return contentValues;
    }
}
