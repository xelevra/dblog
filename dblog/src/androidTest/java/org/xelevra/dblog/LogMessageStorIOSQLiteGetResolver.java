package org.xelevra.dblog;

import android.database.Cursor;
import android.support.annotation.NonNull;
import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;

/**
 * Generated resolver for Get Operation
 */
class LogMessageStorIOSQLiteGetResolver extends DefaultGetResolver<LogMessage> {
    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public LogMessage mapFromCursor(@NonNull Cursor cursor) {
        LogMessage object = new LogMessage();

        object.message = cursor.getString(cursor.getColumnIndex("c_message"));
        object.tag = cursor.getString(cursor.getColumnIndex("c_tag"));
        object.timestamp = cursor.getLong(cursor.getColumnIndex("C_timestamp"));

        return object;
    }
}
